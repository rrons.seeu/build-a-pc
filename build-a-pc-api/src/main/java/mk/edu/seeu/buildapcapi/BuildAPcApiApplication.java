package mk.edu.seeu.buildapcapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildAPcApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildAPcApiApplication.class, args);
    }

}
